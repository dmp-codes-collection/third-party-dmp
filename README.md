# third-party-dmp

A list of open-source DMP implementations. 

For each implementation, we specify the type of DMP, the author, the url to download the code, the used programming language, and the reference paper(s). We also provide a short description of the key features.

Since research on DMPs is still very active, we believe that the list of open-source implementations may keep growing and are welcome to update it in case of requests from the authors. 

Please acknowledge other people work if you use one of the listed implementations.

| Approach | Author & Reference | Repository | Description |
| ------  | ------ | ------ | ------ |
| Classical DMP | [Stefan Schaal](https://doi.org/10.1162/NECO_a_00393) | [`Matlab`](https://gitlab.com/dmp-codes-collection/dmp-classical) | An implementation of the classical discrete and periodic DMPs. |
| Classical DMP | [Freek Stulp](https://doi.org/10.21105/joss.01225) | [`C++` and `Python`](https://github.com/stulp/dmpbbo) | DMP Black-Box Optimization (BBO) (DMPBBO) provides an implementation of the discrete and periodic DMPs. An implementation of BBO with evolution strategies is also provided. Examples show how to use BBO to optimize the parameters of the DMPs. |
| Multi-dimensional DMP| Scott Niekum | [`C++` (`Python`)](https://github.com/sniekum/dmp) | An implementation of the multi-dimensional DMP presented by [(Pastor et al., 2009)](https://doi.org/10.1109/ROBOT.2009.5152385). The implementation is robot-agnostic but integrated into a [ROS](http://wiki.ros.org/dmp) node. The implementation is in `C++`, but `Python` can be used to learn a DMP and generate the motion. |
| Periodic DMP | [Tadej Petrič](https://doi.org/10.1016/j.robot.2015.09.011)  | [`Matlab`/`Simulink`](https://gitlab.com/tpetric/pDMP_MatlabLib) | An implementation for periodic DMP based on the work in [(Gams et al., 2016)](https://doi.org/10.1016/j.robot.2015.09.011). |
| DMP+ | [Jing Wu](https://doi.org/10.1109/IROS.2016.7759554) | [`Python`](https://github.com/jingwuouo/dmp_writing) | An implementation for DMP+ [(Wang et al., 2016)](https://doi.org/10.1109/IROS.2016.7759554) to imitate human writing. | 
| Extended DMP | [Sylvain Calinon](https://doi.org/10.1109/ICHR.2009.5379592) | [`Matlab`](https://gitlab.idiap.ch/rli/pbdlib-matlab/) | An implementation of several programming by demonstration algorithms developed by Sylvain Calinon's research group. There is also an implementation of the extended DMP in [(Calinon et al., 2009)](https://doi.org/10.1109/ICHR.2009.5379592). |
| Quaternion DMP and Mülling's DMP | [Alexander Fabisch (adapted from Arne Böckmann)](https://journals.sagepub.com/doi/10.1177/1729881420913741) | [`C++` and `Python`](https://github.com/rock-learning/bolero) | Implementation of Cartesian DMP based on [(Ude et al., 2014)](https://doi.org/10.1109/ICRA.2014.6907291) and [(Mülling et al., 2010)](https://doi.org/10.1109/ICHR.2010.5686298) that allows to set a final velocity as a metaparameter. |
| Multi-dimensional DMP| Alexander Fabisch | [`Python`](https://github.com/AlexanderFabisch/PyDMP) | An implementation of the multi-dimensional DMP presented by [(Pastor et al., 2009)](https://doi.org/10.1109/ROBOT.2009.5152385). |
| DMP with collision avoidance| [Michele Ginesi](https://doi.org/10.1007/s10846-021-01344-y) | [`Python`](https://github.com/mginesi/dmp_vol_obst) | An implementation of the DMP with volumetric obstacle avoidance algorithm presented by [(Ginesi et al., 2019)](https://doi.org/10.1109/ICAR46387.2019.8981552) and [(Ginesi et al., 2021)](https://doi.org/10.1007/s10846-021-01344-y)|.
| Affine invariant DMP| [Michele Ginesi](https://doi.org/10.1016/j.robot.2021.103844) | [`Python`](https://github.com/mginesi/dmp_pp) | An implementation of the DMP with mollifier-like basis functions and affine inveriance presented by [(Ginesi et al., 2021)](https://doi.org/10.1016/j.robot.2021.103844)|.
